#pragma checksum "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\System\QuickEditForm.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a710ef0a3e6723d1860177403bb31266cf128d0e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Page_System_QuickEditForm), @"mvc.1.0.view", @"/Views/Page/System/QuickEditForm.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Page/System/QuickEditForm.cshtml", typeof(AspNetCore.Views_Page_System_QuickEditForm))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 4 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\System\QuickEditForm.cshtml"
using Rookey.Frame.Common;

#line default
#line hidden
#line 5 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\System\QuickEditForm.cshtml"
using Rookey.Frame.UIOperate;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a710ef0a3e6723d1860177403bb31266cf128d0e", @"/Views/Page/System/QuickEditForm.cshtml")]
    public class Views_Page_System_QuickEditForm : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\System\QuickEditForm.cshtml"
  
    ViewBag.Title = "QuickEditForm";

#line default
#line hidden
#line 6 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\System\QuickEditForm.cshtml"
   UIFrameFactory frameFactory = UIFrameFactory.GetInstance(this.Context.Request);
   Guid moduleId = UIOperate.GetModuleIdByRequest(this.Context.Request);
   Guid roleId = this.Context.Request.Query["roleId"].ObjToGuid();
   Guid? formId = this.Context.Request.Query["formId"].ObjToGuidNull();

#line default
#line hidden
            BeginContext(407, 69, false);
#line 10 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\System\QuickEditForm.cshtml"
Write(Html.Raw(frameFactory.GetQuickEditFormHTML(moduleId, roleId, formId)));

#line default
#line hidden
            EndContext();
            BeginContext(476, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
