#pragma checksum "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\Desktop\DesktopIndex.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9a533ccd933dc977d27bd6b9502e1e011990608f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Page_Desktop_DesktopIndex), @"mvc.1.0.view", @"/Views/Page/Desktop/DesktopIndex.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Page/Desktop/DesktopIndex.cshtml", typeof(AspNetCore.Views_Page_Desktop_DesktopIndex))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 4 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\Desktop\DesktopIndex.cshtml"
using Rookey.Frame.Common;

#line default
#line hidden
#line 20 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\Desktop\DesktopIndex.cshtml"
using Rookey.Frame.UIOperate;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9a533ccd933dc977d27bd6b9502e1e011990608f", @"/Views/Page/Desktop/DesktopIndex.cshtml")]
    public class Views_Page_Desktop_DesktopIndex : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\Desktop\DesktopIndex.cshtml"
  
    ViewBag.Title = "DesktopIndex";

#line default
#line hidden
#line 5 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\Desktop\DesktopIndex.cshtml"
  bool isUiMark = WebConfigHelper.GetAppSettingValue("UIMarkStyle") == "true";

#line default
#line hidden
            DefineSection("styles", async() => {
                BeginContext(170, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 7 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\Desktop\DesktopIndex.cshtml"
     if (isUiMark)
    { 

#line default
#line hidden
                BeginContext(200, 290, true);
                WriteLiteral(@"        <style type=""text/css"">
            .tabs-header {
                background-color: #e3e3e3;
                border-width: 1px;
                border-style: solid;
                border-bottom-width: 0;
                position: relative;
            }
        </style>
");
                EndContext();
#line 18 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\Desktop\DesktopIndex.cshtml"
    }

#line default
#line hidden
            }
            );
#line 21 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\Desktop\DesktopIndex.cshtml"
   UIFrameFactory frameFactory = UIFrameFactory.GetInstance(this.Context.Request);

#line default
#line hidden
            BeginContext(618, 44, false);
#line 22 "D:\wdx\平台\RookeyFrameCore\Rookey.FrameCore.Web\Views\Page\Desktop\DesktopIndex.cshtml"
Write(Html.Raw(frameFactory.GetDesktopIndexHTML()));

#line default
#line hidden
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
